<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PostController@index');
Route::get('/add-post', 'PostController@create');
Route::post('/store', 'PostController@store');
Route::get('/post/{post}', 'PostController@showFull');
Route::get('/delete/{post}', 'PostController@destroy');
Route::get('/update/{post}', 'PostController@edit');
Route::patch('/storeupdate/{post}', 'PostController@update');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
