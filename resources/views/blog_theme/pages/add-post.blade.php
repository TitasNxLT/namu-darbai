@extends('blog_theme/main')

@section('content')
@include('blog_theme._partials.errors')
<form action="/store" method="post" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="title">Pavadinimas</label>
        <input type="text" class="form-control" id="title" name="title">
    </div>
    <div class="form-group">
        <label for="content">Turinys</label>
        <textarea class="form-control" id="content" name="content" rows="3"></textarea>
    </div>
    <div class="form-group custom-file offset-md-3 col-md-6 mb-3 mb-md-0">
        <input type="file" class="custom-file-input text-black" id="listingImage" name="img">
        <label class="custom-file-label text-black" for="listingImage" lang="lt">Pasirinkite failą</label>
    </div>
    <button type="submit" class="btn btn-primary">Saugoti</button>
</form>
@endsection