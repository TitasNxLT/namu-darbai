@extends('blog_theme/main')

@section('content')
<div class="row">
    <div class="col-lg-8 col-md-10 mx-auto">
        <div class="post-preview">
            <a href="post.html">
                <h2 class="post-title">{{ $post->title }}</h2>
                <h3 class="post-subtitle">{{ $post->content }}</h3>
            </a>
            <p class="post-meta">
                {{ $post->created_at }}
            </p>
            <div class="actions">
                <ul>
                    <li><a href="/delete/{{$post->id}}">Šalinti</a></li>
                    <li><a href="/update/{{$post->id}}">Atnaujinti</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection