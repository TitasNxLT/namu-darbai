@extends('blog_theme/main')

@section('content')
<div class="row">
    <div class="col-lg-8 col-md-10 mx-auto">
        @foreach ($posts as $post)
        <div class="post-preview">
            <a href="post.html">
                <h2 class="post-title">{{ $post->title }}</h2>
                <div>
                    <img src="{{asset('storage/'.$post->img)}}" alt="Post image">
                </div>
                <h3 class="post-subtitle">{{ Str::limit($post->content, 100) }}</h3>
                <a href="post/{{$post->id}}">Skaityti daugiau...</a>
            </a>
            <p class="post-meta">
                {{ $post->created_at }}
            </p>
        </div>
        <hr />
        @endforeach
        <!-- Pager-->
        <div class="clearfix"><a class="btn btn-primary float-right" href="#!">Older Posts →</a></div>
    </div>
</div>
@endsection